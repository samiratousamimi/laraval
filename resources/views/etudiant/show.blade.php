@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Etudiant</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
					<table>
						<tr>
							<th>Nom</th><td>{{ $etudiant->nom }}</td>
						</tr>
						<tr>
							<th>Prenom</th><td>{{ $etudiant->prenom }}</td>
						</tr>
					</table>
				 <a href= "{{ route('editEtudiant',$etudiant->id)}}" >{{ trans('Ajouter')}}</a>
				 <a href= "{{ route('deleteEtudiant',$etudiant->id)}}" >{{ trans('Supprimer')}}</a>
				 <a href= "{{ route('updateEtudiant',$etudiant->id)}}" >{{ trans('Modifier')}}</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
