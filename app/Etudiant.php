<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etudiant extends Model
{
    //permet au model d'accepter de ne pas planter lors d'un create par exmeple et de prendre qui les données qui l'interesse 
    //Etudiant : : create($request->all()
	// où on lui envoi toutes données du request genre le token qui ne fait pas partie du model 
	protected $guarded = array() ;
	// indique que la table mappeé par la classe Etudiant n'a pas de champs created et updated_at 
	public $timestamps = false ;
	}

