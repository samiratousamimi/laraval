<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Etudiant;
class EtudiantController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this -> middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
/**
*ajouter un étudiant.
*
@return sur la vue précédente
*/
public function add(Request $request){
Etudiant::create($request->all());
#return view('home');
return back()->with('status' , trans('etudiant.msgenregistrementok')); 
}
/**
*Afficher les informations dun étudiant.
*
*@param int $id : id de l'étudiant 
*@return view :show 
*/
	public function show($id) {
  $etudiant = Etudiant::findorfail($id);
  return view('etudiant.show', compact('etudiant'));
  }
  /**
  Afficher le formulaire pour la modification 
  *
  *
  *@param int $id: id de l'edutiant 
  *
  *@return view : vue edit d'etudiant 
  */
  public function edit($id){
  $etudiant = Etudiant::findorfail($id);// si lid nexiste pas , il retourne une erreur 404
  return view('etudiant.edit' , compact('etudiant'));
  }
  /**
  *valider la modification sur le formulaire
  * 
  *@param int $id (id  l'etudiant) , request ce qui a été les inputs
  * @rturn sur la vue precédente
  */
  public function update(Request $request, $id){
  $etudiant = Etudiant::findorfail($id); //si lid nexiste pas , il retourne une erreur 404
  $etudiant->nom = $request->input('nom');
  $etudiant->prenom = $request->input('prenom');
  $etudiant -> save();
	return back()->with('status',trans('etudiant.msgmiseajourok'));
	}

	public function delete($id){
   Etudiant::destroy($id);
  
  return  view('home');
}
}

